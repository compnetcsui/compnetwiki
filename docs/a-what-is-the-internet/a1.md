## What is The Internet?
![Internet](./img/internet.jpg)
<br><sub>Internet Illustration Credit: [stori](https://depositphotos.com/portfolio-1062624.html)</sub>
<br><br>The Internet is a global network of billions of computers and other electronic devices. [^1] That system facilitates worldwide communication and access to data resources through a vast collection of private, public, business, academic and government networks. [^2] It is governed by agencies like the Internet Assigned Numbers Authority (IANA) that establish universal protocols.
In order for a computer to communicate on the Internet, a set of rules or protocols computers must follow to exchange messages was developed. The two most important protocols allowing computers to transmit data on the Internet are Transmission Control Protocol (TCP) and Internet Protocol (IP).[^3]
<br><br>**World Wide Web**
<br><br>Many people consider these terms are synonymous, but they are actually not the same. 
The Internet is the collection of the many different systems and protocols.[^3] On the other hand, the World Wide Web or commonly refer to as Web or WWW, is one of the protocol of the Internet.
To browse or access the web, you will need an internet browser.

## A Brief History of The Internet
The internet as we know today, traces its origins to one big problem:

![sputnik](./img/sputnik.jpg)  
<sub> _Sputnik Satellite_  
Credit: Detlev van Ravenswaay / Getty </sub>  

**_The Soviets launched Sputnik satellite, and it scared Americans._** [^4]

It was in the midst of the Cold War, October 4 1957, the first man made satellite was launched into orbits. American scientists and military experts were especially concerned about what might happen if Soviets were to attack the nation's telephone system. _"Just one missile"_, they feared. After the Sputnik wake up call, the space race began. In 1958 the US Administration funded various agencies, one of them being ARPA. [^5] ARPA gave birth to the forerunner of modern internet, [**ARPAnet**](https://en.wikipedia.org/wiki/ARPANET).

The answer to the big problem was [**packet switching**](https://en.wikipedia.org/wiki/Packet_switching). In 1965, Paul Baran developed a safer technique of communication [^6]. Instead of sending data as one big stream, the data is broken down into tiny packets that might take different routes to the destination. This way, if a nuclear attack destroys part of the network, the packet will simply take another route.

As packet-switched computer networks multiplied, however, it became more difficult for them to integrate into a single worldwide “internet.” By the end of the 1970s, Vinton Cerf and Bob Khan had begun to work on the design of what we now call the Internet. He called his invention Transmission Control Protocol and Internet Protocol, otherwise known as [**TCP/IP**](https://en.wikipedia.org/wiki/Internet_protocol_suite) [^7]. Cerf’s protocol transformed the internet into a worldwide network.

However, in 1991 the internet changed again. [^8] Tim Berners-Lee, a programmer from Switzerland introduced the [**World Wide Web**](https://webfoundation.org/about/vision/history-of-the-web/) : an internet that was not simply a way to send files from one place to another but was itself a “web” of information that anyone on the Internet could retrieve. Berners-Lee created the Internet that we know today.

## Internet vs Intranet
Just by hearing and looking at both words, it sounds pretty similar and maybe we think that both look alike. In reality, they are two different things with their own unique functions.

**_DEFINITION_**

The **Internet** is a globally-connected network of computers that enables people to share information and communicate with each other. An **Intranet**, on the other hand, is a local or restricted network that enables people to store, organize, and share information within an organization.[^9]

So, an intranet is a restricted version of the internet, and one that doesn’t allow access to anyone outside its network. Its network comprises of all of its employees and senior level, and in some cases, partners, investors, and shareholders. Intranets typically use a local-only network, which restricts access. Only users directly wired to the system can log on. Despite this, remote workers can gain access from home, and employees on the road can access it via mobile.[^10]

![Igloo](https://s24787.pcdn.co/wp-content/uploads/2020/12/94-inarticle-callout-chart.jpg)
<sub> _Internet, Extranet, and Intranet_
<br>
Credit: Igloo Software </sub>

## Examples of Internet and Intranet
To reiterate, The basic difference between the Internet and the Intranet is that the former is accessible to every user all over the world, whereas the latter is more isolated as the Intranet are usually used to connect **only** said computers together and provides access to files within that specific network.

To make it more clear, here are some examples regarding both the Internet and the Intranet.

### Internet Examples
One of the most popular web search engine, Google (http://www.google.com), allows users to search for text, images, and more.
<br>
<img src="./img/google_homepage.PNG" width="720" height="390">
<br>
<sub>_Google Homepage shown in Firefox Web Browser_</sub>

Another example would be a website for version control in software development called GitHub (https://www.github.com/).
<br>
<img src="./img/github_homepage.PNG" width="720" height="390">
<br>
<sub>_GitHub shown in Firefox Web Browser_</sub>

### Intranet Examples
An example of the use of Intranet would be an academic organization website for students and staffs of Universitas Indonesia (http://scele.cs.ui.ac.id) to coordinate lectures, assignments, and also forums for feedbacks, opportunities, and questions. **Notice that the website does not have the World Wide Web service (no "www." at the start) and are inaccessible if forced to use with "www."**
<br>
<img src="./img/scele_homepage.PNG" width="720" height="390">
<br>
<sub>_SCELE Homepage shown in Firefox Web Browser_</sub>

An additional example is a website dedicated for the Human Resources (HR) section in a corporate setting.
<br>
<img src="./img/hrweb_sample_webpage.png" width="720" height="390">
<br>
<sub>_A sample illustration of a website for HR in a corporation_</sub>


## References
[^1]:  GCFGlobal(n.d.). _Internet Basics - What is the Internet?_. Retrieved February 26, 2020 from https://edu.gcfglobal.org/en/internetbasics/what-is-the-internet/1/
[^2]:  Technopedia(n.d.). _Internet_. Retrieved February 26, 2020 from https://www.techopedia.com/definition/2419/internet
[^3]:  FCIT (n.d.). _Internet Basics_. Retrieved February 26, 2020 from https://fcit.usf.edu/internet/chap1/chap1.htm
[^4]:  Kay, Sean (April–May 2013). "America's Sputnik Moments". _Survival_. 55 (2): 123–146. doi:10.1080/00396338.2013.784470. S2CID 154455156
[^5]: "Data Communications at the National Physical Laboratory (1965–1975)", Martin Campbell-Kelly, _IEEE Annals of the History of Computing_, Volume 9 Issue 3–4 (July–Sept 1987), pp. 221–247. Retrieved 18 May 2015
[^6]:  "A Flaw In The Design". _The Washington Post_. May 30, 2015. Historians credit seminal insights to Welsh scientist Donald W. Davies and American engineer Paul Baran
[^7]:  "RFC 675 – Specification of internet transmission control program". Tools.ietf.org. Retrieved May 28, 2009
[^8]: "The Early World Wide Web at SLAC". _The Early World Wide Web at SLAC: Documentation of the Early Web at SLAC._ Retrieved November 25, 2005.
[^9]: Duffy, S. (2020, December 4). _Internet vs. Intranet vs. Extranet: What’s the Difference?_ Retrieved from https://www.igloosoftware.com/blog/internet-vs-intranet-vs-extranet-whats-the-difference/
[^10]: Berry, L. (2019, November 13). _Intranet vs. internet: what’s the difference, and why does it matter?_ Retrieved from https://www.interact-intranet.com/blog/intranet-vs-internet-difference/


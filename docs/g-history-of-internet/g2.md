The history of the internet in Indonesia  began in the early 1990s (Baan, 2017). At that time, the purpose of the internet was to support academic work (Samik-Ibrahim, 2021b). Subsequently, internet service became commercial at 1994 (Baan, 2017). The first internet service provider to provide this service was IndoNet (Baan, 2017). Quoting from [statista.com](https://www.statista.com/statistics/262966/number-of-internet-users-in-selected-countries/), in 2019, the internet in Indonesia continues to grow and even has more than 171 million Internet users.

## The Beginning of the Internet in Indonesia
Citing from Baan's article (2017), it is known that the beginning of the internet in Indonesia starts in the early 1990s. At that time, the first computer to be registered into the internet network came from Universitas Indonesia. The University registered it in 24 June 1988. Internet protocol address of the computer is 192.41.206/24 with the name UI-NETLAB.

![](https://i.imgur.com/7Nw7Chs.jpg)
<div align="center">

Some of the Founding Figures from Left to Right:
[Samik-Ibrahim](https://rse.cs.ui.ac.id/?open=staff/index), [Adisoemarta](https://ikhsanrpg.wordpress.com/2011/07/03/sejarah-internet-indonesia/), [Surya](https://www.cnnindonesia.com/ekonomi/20180714123538-97-314000/dirjen-kemenperin-putu-suryawirawan-gemar-berkemudi-di-hutan), [Hazairin](https://www.linkedin.com/in/arman-hazairin-1b12691/), [Purbo](https://www.satu-indonesia.com/satu/satuindonesiaawards/juri/onno-w-purbo/)

</div>

<br/>

Based on Samik-Ibrahim (2021a) and Baan (2017), some of the pioneers of the internet at that time were:
1. R. M. Samik-Ibrahim: operating systems expert.
2. Suryono Adisoemarta: packet radio & TCP/IP expert.
3. Putu Surya: key figure in the Ministry Industrialization and Development in Indonesia.
4. Arman Hazairin: computer network expert.
5. Onno W. Purbo: the provocator of the first internet in Indonesia.

Quoting from Samik-Ibrahim's article (2021b), the project carried out for the first internet development was UNINET (An Inter-University Computer Network). The goal of the project was to connect 45 state universities under the Ministry of Education and Culture in Indonesia. In addition, UNINET project also had an objective to support education, research, and management of higher education in Indonesia.

## Indonesia's Internet Development in Chronological Order
This chapter will discuss Indonesia's Internet Development starting from the emergence of the Internet to the public in a global scale, and then moving on to Indonesia's early-stage of Internet development, and so on.

### Global Internet Development
The Internet started as a program developed by researchers in collaboration with the government since 1966. The program is restricted to the parties referred previously up until 1980s, where CompuServe and America On Line (AOL) saw the opportunities for The Internet to be used by general masses. The two companies began to offer access to The Internet, such as e-mail interchange, but full access to the Internet was not readily available to the general public.

On 1989, there emerged the first commercialized [ISP](https://www.britannica.com/technology/Internet-service-provider) named [The World](). Developed in Australia, and the United States, The World offered dial-up connections using the public telephone. This promoted the emergence of many others public ISP at that time.

<div align="center">

![](img/the-world.jpg)
<br> *The World ISP (1989)*

</div>


### Early-stage of Indonesia's Internet Development
As explained in the previous chapter, the first Internet Protocol (IP) was initiated by a group of people at the University of Indonesia that went by the name UI-NETLAB. Officially registered by the University of Indonesia at 1988, it was the first official initiative on the sector of Internet and Networking. Thus marked the start of democratization of Internet access in Indonesia.

<div align="center">

![](img/ui-netlab.jpeg)
<br> *UI NETLAB (1988)*

</div>

In the early of 1990s, inspired by UI-NETLAB initiatives and motivated by other factors such as the expensive resources that was required in order to be connected to UI-NETLAB's Internet, academics in Bandung Institute of Technology (ITB) came up with an alternative. A member of Amateur Radio Club (ARC) ITB, Suryono Adisoemarta, had successfully created a connection to Ipteknet (National Network Information founded in 1971) through LAPAN.

<div align="center">

![](img/arc-itb.jpeg)
<br> *ARC ITB (1989)*

</div>

### Commercialization Era of Indonesia's Internet Service Provider

In majority, the subjects involved in the development of ISP in Indonesia was separated into two parties: private companies and government/state-owned companies. This section will discuss the development of each parties separately.

#### The Emergence of Private Commercialized ISP
On 1994, an IBM engineer named Sanjaya created the very first commercialized ISP in Indonesia named IndoNet. It took about five years until Indonesia had its own commercialized ISP since The World (world's first commercialized ISP) was created back in 1989.

<div align="center">

![](img/indonet.jpg)
<br> *IndoNet (1994)*

</div>

A year later, another commercialized ISP was created by the name PT Rahardjasa Internet (RadNet). It went on as IndoNet's first business competitor in terms of commercialized ISP. Later the very same year, ITB student Rully Harbani created PT Melvar Lintasnusa as Indonesia's third commercialized ISP and the first in Bandung at that time.

<div align="center">

![](img/radnet.png)
<br> *RadNet (1995)*

</div>

Seeing the rapid development of private commercialized ISP business in Indonesia, the government then went on to create a set of regulation in 1996 that controls the industry of commercialized ISP.

#### State-owned Commercialized ISP
The first state-owned ISP was belonged to PT Pos Indonesia in 1996. Went by the name Wasantara Net (W-Net), it was a subproject of a larger initiative named Nusantara-21 that was created with the intention to provide Internet access to 300 sub-district (kecamatan) throughout Indonesia.

<div align="center">

![](img/wasantara-net.jpeg)
<br> *W-Net (1996)*

</div>

Not long after W-Net was established, PT Indosat Tbk. also created their own brand of commercialized ISP named IndosatNet. IndosatNet was the most popular ISP in 1990s, this was due to the fact that W-Net, although its wide range of network, was considered very slow at that time.

<div align="center">

![](img/indosat.png)
<br> *IndosatNet (1996)*

</div>

And then in 1998, PT Telekomunikasi Indonesia Tbk. (Telkom) came up with a new ISP with a different business model. PT Telkom provided satellite phone and Internet access as one package.

<div align="center">

![](img/telkom.png)
<br> *PT Telkom (1998)*

</div>

#### Indonesia's ISP speed index in 1996
Below are the ISPs that were popular back in 1996. The list includes both private and state-owned commercialized ISP. Align with the description above, Wasantara-Net had the largest connection throughout Indonesia, but didn't really brought satisfaction to its customer due to its slow bandwith speed.

<div align="center">

| Name           | Location | Connection       | Speed     |
|----------------|----------|------------------|-----------|
| IPTEK-NET      | Jakarta  | 6 other cities   | 256 Kbps  |
| ITB            | Bandung  | -                | 2000 Kbps |
| IndoNet        | Jakarta  | 5 other cities   | 256 Kbps  |
| RadNet         | Jakarta  | 2 other cities   | 512 Kbps  |
| SistelIndo     | Jakarta  | 2 other cities   | 768 Kbps  |
| IdOLA-Net      | Jakarta  | 17 other cities  | 512 Kbps  |
| CBN-Net        | Jakarta  | -                | 256 kbps  |
| Sisfo-Net      | Bandung  | -                | 128 Kbps  |
| Wasantara-Net  | Bandung  | 22 other cities  | 128 Kbps  |
| Melsa-Net      | Bandung  | -                | 128 Kbps  |
| Total Bandwith |          |                  | 3432 Kbps |

</div>

<div align="center">

*Speed comparison between Indonesia's ISPs back in 1996*

<br>

</div>

## Indonesia's Internet User Throughout the Years
Internet usage in Indonesia has grown in a quite significant amount over the past few years. In fact, Indonesia is a nation with the largest Internet user-base in Southeast Asia. Indonesia is also on the 4th place of largest internet users in the world.

![Internet Users in Indonesia](https://i.imgur.com/n3HetuR.png)

Early on 1994, Internet User barely found in Indonesia (0,001%).The graph above represent that Internet User in Indonesia has grown exponentially. Quoting from Henri Kasyfi Soemartono, the General Secretary of APJII, in 2019, 171 million peoples in Indonesia has used internet.

In 2020, Chairman of Indonesia Internet Sevice Provider Association (known as APJII) said the increase is driven by the fast distribution of internet infrastructure and massive digital transformation due to Covid-19 pandemic since March 2020. By early 2021, Internet has reached 202,6 million users (73,7% of populations) in Indonesia (Johnson, 2021)

## Internet Café in Indonesia

![](https://i.imgur.com/VMHbvp0.png)
<div align="center">Internet Café on Gunung Putri, Bogor</div>

Internet Café (also known as Warung Internet in bahasa) Business has started since 1996 in Indonesia. The first Internet Café is BoNet Cafe, opened a year after Bogor Internet (BoNet) ISP was establised. At that time, PC was expensive so most people rather go to Internet Café. Internet Café charge as a time-based rate. In 20 May 2000, Asosiasi Warnet Indonesia (AWARI) was formed (Zulkifli, 2013). Since then, Internet Café was scattered in Indonesia. These things led the growth of Internet User in Indonesia.

## References
- 6 Fakta Mengejutkan Sejarah Internet di Indonesia yang Wajib Disimak. (2019, March 19). Bakti Kominfo. https://www.baktikominfo.id/id/informasi/pengetahuan/6_fakta_mengejutkan_sejarah_internet_di_indonesia_yang_wajib_disimak-768
- Baan, M. (2017, June 19). *Sejak Kapan MASYARAKAT Indonesia Nikmati internet?* stei.itb.ac.id. https://stei.itb.ac.id/id/blog/2017/06/19/sejak-kapan-masyarakat-indonesia-nikmati-internet/.
- Britannica, T. Editors of Encyclopaedia (2018, March 13). Internet service provider. Encyclopedia Britannica. https://www.britannica.com/technology/Internet-service-provider
- Internet service provider. (2021, February 14). Retrieved February 28, 2021, from https://en.wikipedia.org/wiki/Internet_service_provider
- Irso. (2020, November 9). Dirjen PPI: Survei Penetrasi Pengguna Internet di Indonesia Bagian Penting dari Transformasi Digital. https://www.kominfo.go.id/content/detail/30653/dirjen-ppi-survei-penetrasi-pengguna-internet-di-indonesia-bagian-penting-dari-transformasi-digital/0/berita_satker
- Jatmiko, L. D. (2020, November 10). APJII: 196,7 Juta Warga Indonesia Sudah Melek Internet. https://teknologi.bisnis.com/read/20201110/101/1315765/apjii-1967-juta-warga-indonesia-sudah-melek-internet
- Johnson, Joseph. (2021, January 27). Countries with the highest number of internet users 2019.
https://www.statista.com/statistics/262966/number-of-internet-users-in-selected-countries/
- Kemp, Simon. (2021, February 11). DIGITAL 2021: INDONESIA. https://datareportal.com/reports/digital-2021-indonesia
- Merdeka. (2019, October 16). *Awal Mula Internet Masuk Indonesia*. Retrieved February 24, 2021, from https://www.merdeka.com/teknologi/awal-mula-internet-masuk-indonesia.html
- Samik-Ibrahim, R. M. (2021a, January 29). The VauLSMorg (vlsm.org) Archive. https://rms46.vlsm.org/2/122.html.
- Samik-Ibrahim, R. M. (2021b, January 29). The VauLSMorg (vlsm.org) Archive. https://rms46.vlsm.org/1/81.html.
- Saputra, F. A. (2003). Internet Development in Indonesia: A Preview and Perception.
- Validnews. (2020, September 21). *Jos Luhukay Memboyong Internet Ke Indonesia*. Retrieved February 28, 2021, from https://www.validnews.id/Jos-Luhukay--Memboyong-Internet-Ke-Indonesia-Mza
- Zulkifli. (2013). Warung Internet: Gerbang Dunia Virtual Remaja Kota Medan. https://media.neliti.com/media/publications/78695-ID-warung-internet-gerbang-dunia-virtual-re.pdf